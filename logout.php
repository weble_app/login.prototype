<?php
session_start();

require_once 'bootstrap.php';

if (isset($_SESSION['user_id'], $_SESSION['session_id'])) {
    // Находим все ключи пользователя
    $keys = $redis->keys('user:'.$_SESSION['user_id'].':*');

    // Удаляем все ключи сессии пользователя
    if (count($keys)) foreach ($keys as $key) {
        $redis->del($key);
    }

    session_destroy();
}

header('Location: /');

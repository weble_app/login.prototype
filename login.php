<?php
session_start();

require_once 'bootstrap.php';

if (isset($_POST['email'], $_POST['password'])) {
    $user = $db->query("SELECT * FROM users WHERE email = '{$_POST['email']}'")->fetchObject();

    if ($user && $hash->check($_POST['password'], $user->password)) {
        // Генерим ключ сессии. Значение не важно, важно что бы он был просто уникальным,
        // так скажем для этого пользователя в рамках этого сайта
        $session_id = md5('вообще любая строка'.$_SERVER['REQUEST_TIME'].rand(0,9999));

        // Создаем ему запись. Кстати, содержимое можно эксплуатироват,
        // что бы хранить там доп. данные в рамках сессии.
        // Формат очень важен: "user:USER_ID:SESSION_ID"
        $redis->set('user:'.$user->id.':'.$session_id, '[]');

        // Пишем это все в сессию, что бы потом проверять
        $_SESSION['user_id'] = $user->id;
        $_SESSION['session_id'] = $session_id;
    }
}

header('Location: /');

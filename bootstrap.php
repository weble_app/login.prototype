<?php

require_once 'vendor/autoload.php';

/*
 * Hasher
 */

$hash = new \Illuminate\Hashing\BcryptHasher();

/*
 * Redis
 */

$redisConfig = [
    'cluster' => false,
    'default' => [
        'host' => '127.0.0.1',
        'port' => 6379,
        'database' => 0,
    ],
];

$redis = new Illuminate\Redis\Database($redisConfig);

/*
 * PDO
 */

$mysqlConfig = [
    'dsn' => 'mysql:dbname=servers;host=95.85.52.113',
    'user' => 'auth',
    'password' => '1234',
];

$db = new PDO($mysqlConfig['dsn'], $mysqlConfig['user'], $mysqlConfig['password']);

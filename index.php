<?php
session_start();

require_once 'bootstrap.php';

if (isset($_SESSION['user_id'], $_SESSION['session_id'])) {

    // Ищем пользовательскую сессию
    $value = $redis->get("user:{$_SESSION['user_id']}:{$_SESSION['session_id']}");

    // Если сессии пользователя нет, кто-то ее завершил, то завершаем ее и в рамках сайта
    if (is_null($value)) {

        // В данном случаи я просто кидаю на логаут и там все чистится, но как это делать -
        // значения не имет, но выйти надо обязательно.
        header('Location: /logout.php');
        exit;
    }
}
?>

<pre><?php var_dump($_SESSION); ?></pre>

<form method="post" action="/login.php">
    <table>
        <tr>
            <td>Email</td>
            <td><input type="email" name="email"></td>
        </tr>
        <tr>
            <td>Password</td>
            <td><input type="password" name="password"></td>
        </tr>
        <tr>
            <td colspan="2">
                <input type="submit" value="Sign in">
            </td>
        </tr>
    </table>
</form>

<hr/>

<a href="/logout.php">Logout</a>